'use strict';

module.exports = {randomInt, shuffleNames};

function randomInt(lo,hi) {
  return parseInt(Math.random() * (hi - lo) + lo);
}

function shuffleNames(array) {
  let iniIndex = array.length, temp;
  while (0 !== iniIndex) {
    iniIndex--;
    let index = randomInt(0,array.length);
    temp = array[iniIndex];
    array[iniIndex] = array[index];
    array[index] = temp;
  }
  return array;
}
