'use strict';

module.exports = testRandomInt;

const shuf = require('./../randomInt.js');
const assert = require('assert');

function testRandomInt() {
  test_randomInt_with_lower_bound_included();
  test_randomInt_with_upper_bound_excluded();
  test_all_numbers_within_the_range_are_generated();
  test_shuffleNames();
}

function test_shuffleNames() {
  let array = ["Thiru", "aishu", "Thamizh", "Sindhu"];
  let arr=array.slice(0);
  shuf.shuffleNames(array);
  assert.notEqual(array, arr);
}

function test_randomInt_with_lower_bound_included() {
  let i = 0, foundLower = false;
  while(i < 1000) {
    let temp = shuf.randomInt(1,5);
    if (temp === 1) {
      foundLower = true;
      break;
    }
    i++;
  }
  assert.ok(foundLower);
}

function test_randomInt_with_upper_bound_excluded() {
  let i = 0, check = false;
  while (i < 1000) {
    let temp =shuf.randomInt(1,5);
    if (temp === 5) {
      check = true;
      break;
    }
    i++;
  }
  assert.ok(!check);
}

function test_all_numbers_within_the_range_are_generated() {
  let arr = [ false,false,false,false ];
  for (let i = 0; i < 1000; i++) {
    let temp = shuf.randomInt(1,5);
    arr[temp - 1] = true;
  }
  assert.ok(arr[0]);
  assert.ok(arr[1]);
  assert.ok(arr[2]);
  assert.ok(arr[3]);
}

testRandomInt();
